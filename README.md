##Medientechnik - Machine Learning

## Authors
<b>Grafenau Thomas</b><br>
<b>Popp Sascha</b>


# To start this project

<ul>
    <li>npm install</li>
    <li>npm run develop</li>
    <li>choose folder <i>medientechnik_code</i></li>
    <li>maybe press <i>strg+f5</i> to load the setup function</li>
</ul>

## medientechnik_image_detection
<p>
This project detects images. The images are stored in the folder image, and are indicated<br>
over the assets/data.json file. 

To classify images we are using the <i>MobileNet</i> modell from ml5.
</p>

## medientechnik_video_pose_detection
<p>This project detects the pose structure of an object in a videostream.<br>
Here we are using the <i>PoseNet</i> model.
<u>PoseNet is a machine learning model that allows for Real-time Human Pose Estimation.</u>


## medientechnik_video_processing
<p>This project detects object in a videostream. Again we are using the  <i>MobileNet</i> modell from ml5</p>
